(function ($, Drupal) {


  //Header Cover Swiper
  Drupal.behaviors.headSwiper = {
    attach: function(context, settings) {
      if($('.header-slide').length){
        var headSwiper = new Swiper('.header-slide', {
          speed: 700,
          effect: 'fade',
          autoplay: {
            delay: 4000,
          },
          pagination: {
            el: '.header-slide .swiper-pagination',
            type: 'bullets',
            clickable: true
          }
        });
      }
    }
  }

  //Fixed Top Scrolling
  Drupal.behaviors.fixedTop = {
    attach: function(context, settings) {
      var topController = new ScrollMagic.Controller();

      var topSm = new ScrollMagic.Scene({
        triggerElement: '.section_main'
      })
      .setClassToggle('.section_branding','is-scrolling')
      .triggerHook(0.4)
      .addTo(topController);
      /*
      var scrollMenuTl = new TimelineMax();
      var scrollMenuLink = $("#superfish-main > li",context);
      scrollMenuTl.staggerFromTo(scrollMenuLink, 0.3, {autoAlpha:1, y:0},{autoAlpha:0, y:-20, ease: Power1.easeInOut}, 0.2);
      var scrollMenuSm = new ScrollMagic.Scene({
        triggerElement: '.section_main'
      })
      .triggerHook(0.4)
      .setTween(scrollMenuTl)
      .reverse(true)
      .addTo(topController);
      */
    }
  }

  //Paragraph Testo e immagini Swiper
  Drupal.behaviors.txtImgSwiper = {
    attach: function(context, settings) {
      if($('.par-text-image__imgs').length){
        var parTxtImgSwiper = new Swiper('.par-text-image__imgs', {
          speed: 700,
          effect: 'slide',
          loop: true,
          pagination: {
            el: '.par-text-image__imgs .swiper-pagination',
            type: 'bullets',
            clickable: true
          }
        });
      }
    }
  }


})(jQuery, Drupal);
