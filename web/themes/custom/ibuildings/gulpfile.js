var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    prefix      = require('gulp-autoprefixer');


/**
 * @task sass
 * Compile files from scss
*/
gulp.task('sass', function () {
  return gulp.src('scss/**/*')
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix(['last 5 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: false }))
    .pipe(gulp.dest('css'));
});

/**
 * @task watch-scss
 * Watch scss files
 */
gulp.task('watch-scss', ['sass'], function () {
  return gulp.watch(['scss/**/*.scss'], {interval: 2000}, function(){
    setTimeout(function () {
        gulp.start('sass');
    }, 100);
  });
});


/**
 * Default task, running just `gulp` will
 * compile Sass files, launch Browsersync & watch files.
*/
gulp.task('watch', ['watch-scss']);


